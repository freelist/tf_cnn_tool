# TensorFlow Convolutional Neural Network tool for Python

This is a simple tool for Python meant to deal with fast training and testing of a CNN with TensorFlow

## Install

If you are using https:

```
$ git clone https://freelist@bitbucket.org/freelist/tf_cnn_tool.git
```
If you are using ssh:

```
$ git clone git@bitbucket.org:freelist/tf_cnn_tool.git
```

## Usage

It is super-simple.
First you need to generate a .yaml file, containing all the necessary for the execution of the tool. See below for an example:

```yaml
# Learning Parameters
LEARNING_RATE : 0.001
BATCH_SIZE : 50
TRAINING_ITERS : 2000
DISPLAY_STEP : 1

# Network Data Parameters
INPUT_SHAPE : [32, 32, 3] # Input Image shape
OUTPUT_SHAPE : [2] # Total classes as vector

# Mapping of the labels
LABEL_MAPPING_FILE : "/home/freelist/Desktop/training_cnn/mapping.txt"

# Datasets as jpg
JPG_TRAINING_DATA : "/home/freelist/Desktop/training_cnn"
JPG_TEST_DATA : "/home/freelist/Desktop/test_cnn"

# Datasets as csv
TRAINING_DATA : "/home/freelist/Desktop/training_cnn/train.csv"
TEST_DATA : "/home/freelist/Desktop/test_cnn/test.csv"

# Logging
LOG_PATH : "/home/freelist/Desktop/logs_path"
SNAPSHOT_PATH : "/home/freelist/Desktop/snapshots_path"

```

After you need to use the generate_csv.py scripts to translate your jpegs dataset into a format needed by this tool. See below for an example:

```
To complete ...
```
