import numpy as np
#from scipy.ndimage import imread
import cv2
import tensorflow as tf
import yaml
#from vgg_m_net import VGG_M_2048_NET as Network
from spqr_net import SPQR_NET as Network
import glob, os



config = yaml.load(file("config.yaml"))

def get_last_checkpoint_model(checkpoint_path):
    with open(checkpoint_file, 'r') as myfile:
        model_file=myfile.read()[:-1]
    model_file_splitted = model_file.split("\n")
    last_model_file = model_file_splitted[len(model_file_splitted)-1]
    model = last_model_file.split(" ")[1].replace("\"","")
    return model

def predict_once(image_path, model_path):

    # Create model
    x = tf.placeholder(tf.float32, [None] + config["INPUT_SHAPE"])

    prediction_input = np.empty([1] + config["INPUT_SHAPE"], np.uint8)
    #print(prediction_input.shape)
    #prediction_input[0:] = imread(image_path, mode="RGB")
    prediction_input[0,:,:,:] = cv2.imread(image_path, cv2.CV_LOAD_IMAGE_COLOR)

    cv2.imshow("Test Image", prediction_input[0,:,:,:])
    cv2.waitKey(0)

    #net = Network(x, config["OUTPUT_SHAPE"][0])
    net = Network()
    logits = net.build_net(x)
    pred = tf.nn.softmax(logits)

    # Start Prediction
    with tf.Session() as sess:

        saver = tf.train.Saver()
        saver.restore(sess, model_path)

        prediction = sess.run(pred, feed_dict={x: prediction_input})
        #labels must be enumerated as the indices
        label = prediction[0].argmax(axis=0)


    return prediction, label

def predict_folder(images_path, model_path):

    # Create model
    x = tf.placeholder(tf.float32, [None] + config["INPUT_SHAPE"])

    #net = Network(x, config["OUTPUT_SHAPE"][0])
    net = Network()
    logits = net.build_net(x)
    pred = tf.nn.softmax(logits)

    # Start Prediction
    with tf.Session() as sess:

        saver = tf.train.Saver()
        saver.restore(sess, model_path)

        for img in images_path:
            prediction_input = np.empty([1] + config["INPUT_SHAPE"], np.uint8)
            #print(prediction_input.shape)
            #prediction_input[0:] = imread(image_path, mode="RGB")
            prediction_input[0,:,:,:] = cv2.imread(img, cv2.CV_LOAD_IMAGE_COLOR)
            #cv2.imshow("Test Image", prediction_input[0,:,:,:])
            #cv2.waitKey(1)
            prediction = sess.run(pred, feed_dict={x: prediction_input})
            #labels must be enumerated as the indices
            label = prediction[0].argmax(axis=0)
            print "Probabilities: ", prediction
            print "Label: ", label


if __name__ == "__main__":
    #image = "/home/freelist/Desktop/img_reali/test_cnn/Bearing_Box/frame0142_0.jpg"
    #image = "/home/freelist/Desktop/img_reali/test_cnn/Axis/frame0001_0.jpg"
    #reading last model in checkpoints
    checkpoint_file = config["SNAPSHOT_PATH"] + "/" + "checkpoint"
    model = get_last_checkpoint_model(checkpoint_file)
    files_path = glob.glob(os.path.join("/home/freelist/Desktop/img_reali/test_cnn/Axis/", '*.jpg'))
        #print prediction probabilities and label
        #print "Probabilities: ", prob
        #print "Label: ", label
    #perform prediction
    predict_folder(files_path, model)
