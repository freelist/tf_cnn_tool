import yaml
import os
import glob
import cv2
import random
import numpy as np

config = yaml.load(file("config_augmentation.yaml"))

def get_immediate_subdirectories(a_dir):
    return [name for name in os.listdir(a_dir) if os.path.isdir(os.path.join(a_dir, name))]

def random_crop(img):
    # Randomly crop a [height, width] section of the image. - To Do ...
    return None

def flip(img, param):
    # Randomly flip the image horizontally.
    return cv2.flip(img,param)

def adjust_gamma(image, gamma=1.0):
	# build a lookup table mapping the pixel values [0, 255] to
	# their adjusted gamma values
	invGamma = 1.0 / gamma
	table = np.array([((i / 255.0) ** invGamma) * 255
		for i in np.arange(0, 256)]).astype("uint8")

	# apply gamma correction using the lookup table
	return cv2.LUT(image, table)


def augmentDataset():
    imageCount = 0
    subfolds = os.path.join(get_immediate_subdirectories(config["DATASET_MAIN_PATH"]))
    for f in subfolds:
      subfold = config["DATASET_MAIN_PATH"] + '/' + f
      #change the file extensions according to your needs
      files_path = glob.glob(os.path.join(subfold, '*.jpg'))
      for filename in files_path:
          imageCount = imageCount + 1
          file_name = filename[:-4]
          img = cv2.imread(filename, cv2.CV_LOAD_IMAGE_COLOR)
          cv2.imshow("Img - original", img)
          #flip image
          img_h_flipped = flip(img,0)
          cv2.imwrite(file_name + "_h_flipped" + ".jpg",img_h_flipped)
          img_v_flipped = flip(img,1)
          cv2.imwrite(file_name + "_v_flipped" + ".jpg",img_v_flipped)
          img_b_flipped = flip(img,-1)
          cv2.imwrite(file_name + "_b_flipped" + ".jpg",img_b_flipped)

          #bright change imgs
          adjusted_025 = adjust_gamma(img, gamma=0.25)
          cv2.imwrite(file_name + "_adjusted_025" + ".jpg",adjusted_025)
          adjusted_05 = adjust_gamma(img, gamma=0.5)
          cv2.imwrite(file_name + "_adjusted_05" + ".jpg",adjusted_05)
          adjusted_175 = adjust_gamma(img, gamma=1.75)
          cv2.imwrite(file_name + "_adjusted_175" + ".jpg",adjusted_175)
          adjusted_2 = adjust_gamma(img, gamma=2)
          cv2.imwrite(file_name + "_adjusted_2" + ".jpg",adjusted_2)
          adjusted_3 = adjust_gamma(img, gamma=3)
          cv2.imwrite(file_name + "_adjusted_3" + ".jpg",adjusted_3)
          #adjusted_hf_025 = adjust_gamma(img_h_flipped, gamma=0.25)
          #adjusted_hf_05 = adjust_gamma(img_h_flipped, gamma=0.5)
          #adjusted_hf_175 = adjust_gamma(img_h_flipped, gamma=1.75)
          #adjusted_hf_3 = adjust_gamma(img_h_flipped, gamma=3)
          #adjusted_vf_025 = adjust_gamma(img_v_flipped, gamma=0.25)
          #adjusted_vf_05 = adjust_gamma(img_v_flipped, gamma=0.5)
          #adjusted_vf_175 = adjust_gamma(img_v_flipped, gamma=1.75)
          #adjusted_vf_3 = adjust_gamma(img_v_flipped, gamma=3)
          #adjusted_bf_025 = adjust_gamma(img_b_flipped, gamma=0.25)
          #adjusted_bf_05 = adjust_gamma(img_b_flipped, gamma=0.5)
          #adjusted_bf_175 = adjust_gamma(img_b_flipped, gamma=1.75)
          #adjusted_bf_3 = adjust_gamma(img_b_flipped, gamma=3)

          #rescale
          resized_img_2 = cv2.resize(img,None,fx=2, fy=2, interpolation = cv2.INTER_CUBIC)[32:96, 32:96]
          cv2.imwrite(file_name + "_resized_2" + ".jpg",resized_img_2)
          resized_img_4 = cv2.resize(img,None,fx=4, fy=4, interpolation = cv2.INTER_CUBIC)[96:160, 96:160]
          cv2.imwrite(file_name + "_resized_4" + ".jpg",resized_img_4)

          print "image analized: " + filename


if __name__ == "__main__":
    augmentDataset()

