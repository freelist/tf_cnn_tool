import tensorflow as tf
from network import Network

import yaml
config = yaml.load(file("config.yaml"))


class SPQR_NET(Network):
    def __init__(self):
        super(self.__class__, self).__init__()

        # Basic setup
        self.name = "SPQR_NET"
        self.input_shape = config["INPUT_SHAPE"]
        self.output_shape = config["OUTPUT_SHAPE"]

    def build_net(self, input):

        # Make sure input images have the right dimensions
        assert input.get_shape()[1:] == self.input_shape

        # conv(kernel_x, kernel_y, stride_x, stride_y, input size, output size)
        # pool(kernel_x, kernel_y, stride_x, stride_y)
        # fc(output size)
        # dropout(dropout_rate)
        # lrn(depth_radius, bias, alpha, beta)
        # debug()

        (self
            .input(input)
            .conv(3, 3, 1, 1, 3, 64, name="conv1")
            .pool(3, 3, 2, 2)
            .conv(3, 3, 1, 1, 64, 128, name="conv2")
            .pool(3, 3, 2, 2)
            .conv(3, 3, 1, 1, 128, 256, name="conv3")
            .pool(3, 3, 2, 2)
            .fc(1024)
            .dropout(0.5)
            .output([int(numeric_string) for numeric_string in config["OUTPUT_SHAPE"]][0])



            .debug()
        )

        return self.get_last_output()
