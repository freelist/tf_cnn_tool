import yaml
import os
import glob
import random

config = yaml.load(file("config.yaml"))

def get_immediate_subdirectories(a_dir):
    return [name for name in os.listdir(a_dir) if os.path.isdir(os.path.join(a_dir, name))]

def readLabels():
    labelsMappingFile = config["LABEL_MAPPING_FILE"]

    with open(labelsMappingFile, 'r') as myfile:
        labelMappingString=myfile.read()

    labelsMappingString_Splitted = labelMappingString.split("\n")
    labels = {}

    for i in range(0,len(labelsMappingString_Splitted)):
        _class = labelsMappingString_Splitted[i].split("\t")[0].split(" ")[1]
        _label = labelsMappingString_Splitted[i].split("\t")[1].split(" ")[1]
        labels[_class] = _label
    return labels

def transformTrain():
    imageCount = 0
    subfolds = os.path.join(get_immediate_subdirectories(config["JPG_TRAINING_DATA"]))
    csv_file = ''
    csvArray = []
    mappingString = ''
    subfolderCtr = 0
    for f in subfolds:
      subfold = config["JPG_TRAINING_DATA"] + '/' + f
      #change the file extensions according to your needs
      files_path = glob.glob(os.path.join(subfold, '*.jpg'))
      for filename in files_path:
        imageCount = imageCount + 1
        row = ''
        row = row + filename + ',' + str(subfolderCtr) + '\n'
        csvArray.append(row)
      mappingString = mappingString + 'Class: ' + f + '\ttfLabel: ' + str(subfolderCtr) + '\n'
      subfolderCtr = subfolderCtr + 1

    random.shuffle(csvArray)

    for i in range(0,len(csvArray)):
        csv_file = csv_file + csvArray[i]

    print(str(imageCount) + " images found")

    csvFile = open(config["TRAINING_DATA"], 'w')
    csvFile.write(csv_file[:-1])

    mappingFile = open(config["LABEL_MAPPING_FILE"], 'w')
    mappingFile.write(mappingString[:-1])

def transformTest():
    labels = readLabels()
    imageCount = 0
    subfolds = os.path.join(get_immediate_subdirectories(config["JPG_TEST_DATA"]))
    csv_file = ''
    csvArray = []
    subfolderCtr = 0
    for f in subfolds:
      subfold = config["JPG_TEST_DATA"] + '/' + f
      #change the file extensions according to your needs
      files_path = glob.glob(os.path.join(subfold, '*.jpg'))
      for filename in files_path:
        imageCount = imageCount + 1
        row = ''
        row = row + filename + ',' + labels[f] + '\n'
        csvArray.append(row)
      subfolderCtr = subfolderCtr + 1

    random.shuffle(csvArray)

    for i in range(0,len(csvArray)):
        csv_file = csv_file + csvArray[i]

    print(str(imageCount) + " images found")

    csvFile = open(config["TEST_DATA"], 'w')
    csvFile.write(csv_file[:-1])

transformTrain()
transformTest()
